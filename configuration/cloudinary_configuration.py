import os

import cloudinary
import cloudinary.api
import cloudinary.uploader
from werkzeug.utils import secure_filename

cloudinary.config(
    cloud_name="leandro-costa",
    api_key="998576386648918",
    api_secret="L4E1_0cRKc-YMUgGyl4mJ0BvMfg"
)

IMAGE_SIZE = 400, 400

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def upload_file(file, tag):
    filename = secure_filename(file.filename)
    file_path = os.path.join(os.environ.get("UPLOAD_FOLDER"), filename)
    file.save(file_path)
    result = cloudinary.uploader.upload(file_path,
                                        width=200,
                                        height=200,
                                        gravity="face",
                                        zoom=0.65,
                                        crop="thumb",
                                        tags=tag)
    os.remove(file_path)
    return result
