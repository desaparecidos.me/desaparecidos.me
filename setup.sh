#!/usr/bin/env bash
echo "================CREATE VIRTUALENV=============="
virtualenv venv
source venv/bin/activate
echo "=============INSTALL REQUIREMENTS=============="
pip install -r requirements.txt