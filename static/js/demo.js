demo = {
    showNotification: function (from, align, message, message_type) {
        type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];

        color = Math.floor((Math.random() * 6) + 1);

        $.notify({
            icon: "add_alert",
            message: message

        }, {
            type: type[message_type],
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }
}