# Desaparecidos.me

## Portal 

Frontend da api de *desaparecidos.me*

### Objetivo

> Plataforma digital para encontrar pessoas desaparecidas

### Tecnologias e frameworks

> Python 3.6

> Flask

> Cloudinary

### Integrações

* Cloudinary

É necessário a criação de uma conta de teste na mesma para utilizar no desenvolvimento, pois todas nossas imagens não são 
armazenadas na infra estrutura local e sim em bucket s3.

https://cloudinary.com/


### Variaveis de ambiente

```bash
$ export BASE_URL=http://localhost:5001
$ export CLOUDINARY_CLOUD_NAME=""
$ export CLOUDINARY_API_KEY=""
$ export CLOUDINARY_API_SECRET=""
$ export BASIC_AUTH_USERNAME=""
$ export BASIC_AUTH_PASSWORD=""
```

##### Como instalar

```bash
$ pip install -r requirements.txt
```

##### Como executar o projeto

```bash
$ python app.py
```                   
ou
```bash
$ gunicorn app:app
```

##### Dependências

> boto3==1.7.80

> botocore==1.10.80

> CacheControl==0.12.5

> certifi==2018.8.13

> chardet==3.0.4

> click==6.7

> cloudinary==1.12.0

> docutils==0.14

> Flask==1.0.2

> gunicorn==19.9.0

> idna==2.7

> image==1.5.24

> iso3166==0.9

> itsdangerous==0.24

> Jinja2==2.10

> jmespath==0.9.3

> mapbox==0.16.2

> MarkupSafe==1.0

> mock==2.0.0

> msgpack==0.5.6

> pbr==4.2.0

> pika==0.12.0

> polyline==1.3.2

> pymongo==3.7.1

> python-dateutil==2.7.3

> python-http-client==3.1.0

> pytz==2018.5

> requests==2.19.1

> s3transfer==0.1.13

> sendgrid==5.5.0

> six==1.11.0

> uritemplate==3.0.0

> urllib3==1.23

> Werkzeug==0.14.1