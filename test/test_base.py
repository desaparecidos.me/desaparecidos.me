from app import app
from unittest import TestCase


class TestBase(TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.app_context = app.app_context
