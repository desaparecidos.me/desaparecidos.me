from random import random

from faker import Faker


def get_missing():
    fake = Faker()
    return {
        "name": fake.name(),
        "nickname": "beto",
        "height": "",
        "user_id": "",
        "clothes": "Tenis calça jeans e moletom",
        "gender": "male",
        "eyes": "blue",
        "birth_date": "01/01/1993",
        "event_report": "010101",
        "hair": "black",
        "skin": "white",
        "geometry": [
            "-46.870309",
            "-23.492381"
        ],
        "date_of_disappearance": "01/01/1993",
        "status": "disappeared",
        "tattoo_description": "",
        "scar_description": "",
        "photo": "http://res.cloudinary.com/leandro-costa/image/upload/v1535409084/cafikbutjtesi5qbvofg.jpg"
    }
