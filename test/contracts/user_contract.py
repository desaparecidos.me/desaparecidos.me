from faker import Faker


def get_user():
    fake = Faker()
    return {
        "name": fake.name(),
        "email": fake.email(),
        "password": 'abc',
        "phone": "(11)92931-7589",
        "cell_phone": "(11)98037-3099",
        "accept_terms": True,
        "avatar": None
    }


def get_user_uncompleted():
    fake = Faker()
    return {
        "name": fake.name(),
        "password": 'abc',
        "phone": "(11)92931-7589",
        "cell_phone": "(11)98037-3099",
        "accept_terms": True,
        "avatar": None
    }
