from faker import Faker


def get_homeless():
    fake = Faker()
    return {
        "name": fake.name(),
        "nickname": "",
        "birth_date": "01/01/1991",
        "geometry": ["-46.65474", "-23.55322"],
        "eyes": "brown",
        "gender": "male",
        "hair": "brown",
        "skin": "black",
        "photo": "http://res.cloudinary.com/leandro-costa/image/upload/v1535058404/obpawpch94ltfknu0ioh.jpg"
    }
