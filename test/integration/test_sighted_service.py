import services.disappeared_service as missing_service
import services.user_service as user_service
import services.sighted_service as sighted_service
import test.contracts.missing_contract as missing_contract
import test.contracts.user_contract as user_contract
from commons.request_commons import get_by_uri
from test.test_base import TestBase


class SightedServiceTest(TestBase):
    def test_create_sighted(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            missing_object = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, missing_object.status_code)

            sighted = {
                "observation": "Estava dormindo na rua",
                "geometry": [
                    "-46.870309",
                    "-23.492381"
                ]
            }

            sighted_result = sighted_service.create_sighted(missing_object.json()["missing"]["_id"], sighted)
            self.assertEqual(201, sighted_result.status_code)

            missing_result_delete = missing_service.remove_missing(missing_object.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_create_sighted_when_disappeared_not_found(self):
        with self.app_context():
            sighted = {
                "observation": "Estava dormindo na rua",
                "geometry": [
                    "-46.870309",
                    "-23.492381"
                ]
            }

            sighted_result = sighted_service.create_sighted("5b8f1694b8ea92702b5b1729", sighted)
            self.assertEqual(404, sighted_result.status_code)

    def test_find_all_sighted_by_missing_id(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            missing_object = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, missing_object.status_code)

            sighted = {
                "observation": "Estava dormindo na rua",
                "geometry": [
                    "-46.870309",
                    "-23.492381"
                ]
            }

            sighted_result = sighted_service.create_sighted(missing_object.json()["missing"]["_id"], sighted)
            self.assertEqual(201, sighted_result.status_code)

            sighted_list = sighted_service.find_by_missing(missing_object.json()["missing"]["_id"])
            self.assertEqual(200, sighted_list.status_code)
            self.assertEqual(1, len(sighted_list.json()["sighted"]))

            missing_result_delete = missing_service.remove_missing(missing_object.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_find_all_when_missing_not_found(self):
        with self.app_context():
            sighted_result = sighted_service.find_by_missing("5b8f1694b8ea92702b5b1729")
            self.assertEqual(404, sighted_result.status_code)
