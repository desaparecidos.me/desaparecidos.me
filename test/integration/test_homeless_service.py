import services.homeless_service as homeless_service
from test.contracts.homeless_contract import get_homeless
from test.test_base import TestBase


class HomelessServiceTest(TestBase):

    def test_create_homeless(self):
        with self.app_context():
            homeless = get_homeless()
            result = homeless_service.create_homeless(data=homeless)

            self.assertEqual(201, result.status_code)
            self.assertTrue('uri' in result.json())

    def test_get_all_missing(self):
        with self.app_context():
            with self.app_context():
                homeless = get_homeless()
                result = homeless_service.create_homeless(data=homeless)

                self.assertEqual(201, result.status_code)
                self.assertTrue('uri' in result.json())

                homeless_result = homeless_service.find_all_homeless()
                self.assertEqual(200, homeless_result.status_code)
                self.assertTrue(len(homeless_result.json()["homeless"]) > 0)

    def test_get_all_missing(self):
        with self.app_context():
            pass
        # TODO implementing test

    def test_create_homeless_duplicated(self):
        with self.app_context():
            homeless = get_homeless()
            result = homeless_service.create_homeless(data=homeless)

            self.assertEqual(201, result.status_code)
            self.assertTrue('uri' in result.json())

            result = homeless_service.create_homeless(data=homeless)
            self.assertEqual(409, result.status_code)
