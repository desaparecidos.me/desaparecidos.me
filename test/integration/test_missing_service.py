import services.disappeared_service as missing_service
import services.user_service as user_service
import test.contracts.missing_contract as missing_contract
import test.contracts.user_contract as user_contract
from commons.request_commons import get_by_uri
from test.test_base import TestBase


class MissingResourceTest(TestBase):

    def test_create_missing(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_create_missing_when_user_not_found(self):
        with self.app_context():
            _id = "5b8c6bac64cc80fc700a4cca"
            missing = missing_contract.get_missing()

            missing_result = missing_service.update(_id, missing)
            self.assertEqual(404, missing_result.status_code)

    def test_create_missing_with_insufficient_field(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()
            missing.pop("name")

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(400, missing_result.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_update_missing(self):
        with self.app_context():
            user = user_contract.get_user()

            # Create user
            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            # Create missing
            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            # Find missing by uri
            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            # Update missing
            new_missing = missing_contract.get_missing()
            new_missing["user_id"] = result.json()["missing"]["user_id"]
            missing_update = missing_service.update(result.json()["missing"]["_id"], new_missing)
            self.assertEqual(200, missing_update.status_code)
            self.assertEqual(new_missing["name"], missing_update.json()["missing"]["name"])

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_find_missing_by_search(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            find_missing_by_search = missing_service.search_all("black")
            self.assertEqual(200, find_missing_by_search.status_code)
            self.assertTrue('disappeared' in find_missing_by_search.json())

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_find_missing_by_page(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            find_missing_by_pagination = missing_service.pagination(0)
            self.assertEqual(200, find_missing_by_pagination.status_code)
            self.assertTrue('disappeared' in find_missing_by_pagination.json())

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_count_all_missing(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            count_missing = missing_service.count()
            self.assertTrue(count_missing > 0)

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_get_all_geometry(self):
        with self.app_context():
            user = user_contract.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            missing = missing_contract.get_missing()

            missing_result = missing_service.create_missing(create_result.json()["user"]["_id"], missing)
            self.assertEqual(201, missing_result.status_code)

            result = get_by_uri(missing_result.json()["uri"])
            self.assertEqual(200, result.status_code)

            geometry = missing_service.disappeared_geometry()
            self.assertEqual(200, geometry.status_code)
            self.assertTrue(len(geometry.json()["disappeared"]) > 0)

            missing_result_delete = missing_service.remove_missing(result.json()["missing"]["_id"])
            self.assertEqual(204, missing_result_delete.status_code)

            result = user_service.delete_use(create_result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_get_all_geometry(self):
        with self.app_context():
            pass
        # TODO implement test
