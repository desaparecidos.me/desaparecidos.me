import services.user_service as user_service
from test.test_base import TestBase
import test.contracts.user_contract as user_contracts


class UserServiceTest(TestBase):

    def test_create_user(self):
        with self.app_context():
            user = user_contracts.get_user()

            result = user_service.create_user(user)
            self.assertIsNotNone(result.json())
            self.assertEqual(200, result.status_code)
            self.assertEqual(user["name"], result.json()["user"]["name"])

            result = user_service.delete_use(result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_create_user_with_insufficient_fields(self):
        with self.app_context():
            user = user_contracts.get_user_uncompleted()

            result = user_service.create_user(user)
            self.assertEqual(400, result.status_code)

    def test_authentication(self):
        with self.app_context():
            user = user_contracts.get_user()

            result = user_service.create_user(user)
            self.assertIsNotNone(result.json())
            self.assertEqual(200, result.status_code)

            auth = {
                "email": user["email"],
                "password": 'abc'
            }

            auth_result = user_service.authentication(auth)

            self.assertEqual(200, auth_result.status_code)
            self.assertEqual(auth["email"], auth_result.json()["user"]["email"])

            result = user_service.delete_use(result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_authentication_user_not_found(self):
        with self.app_context():
            auth = {
                "email": "teste@gmail.com",
                "password": 'abc'
            }

            auth_result = user_service.authentication(auth)
            self.assertEqual(404, auth_result.status_code)

    def test_remember_password(self):
        with self.app_context():
            user = user_contracts.get_user()

            result = user_service.create_user(user)
            self.assertEqual(200, result.status_code)
            self.assertIsNotNone(result.json())

            remember = user_service.remember_password({"email": user["email"]})
            self.assertEqual(200, remember.status_code)
            self.assertIsNotNone(remember.json())

            result = user_service.delete_use(result.json()["user"]["_id"])
            self.assertEqual(204, result.status_code)

    def test_remember_password_when_email_not_found(self):
        with self.app_context():
            email = "teste@gmail.com"

            remember = user_service.remember_password({"email": email})
            self.assertEqual(404, remember.status_code)

    def test_update_user(self):
        with self.app_context():
            user = user_contracts.get_user()

            create_result = user_service.create_user(user)
            user_result = create_result.json()["user"]
            self.assertIsNotNone(user_result)
            self.assertEqual(200, create_result.status_code)
            self.assertEqual(user["name"], create_result.json()["user"]["name"])

            user_update = user_contracts.get_user()

            update_result = user_service.update_user(user_result["_id"], user_update)
            self.assertIsNotNone(update_result.json())
            self.assertEqual(200, update_result.status_code)
            self.assertEqual(update_result.json()["user"]["email"], user_update["email"])

            result = user_service.delete_use(user_result["_id"])
            self.assertEqual(204, result.status_code)
