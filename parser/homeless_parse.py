from datetime import datetime

from flask import render_template

import parser.basic_parse as basic
from configuration.cloudinary_configuration import allowed_file, upload_file

DEFAULT_GEOMETRY = [-47.8823, -15.7934]


def request_parse(request):
    if 'avatar' not in request.form \
            and 'file' not in request.files \
            and not request.files['file'] \
            and not allowed_file(request.files['file'].filename):
        return render_template("error/image_format.html")

    avatar = None

    if 'avatar' in request.form:
        avatar = request.form.get("avatar")

    if 'file' in request.files:
        file = request.files['file']
        avatar = upload_file(file, 'missing')["url"]

    return {
        "name": request.form.get("name"),
        "nickname": request.form.get("nickname"),
        "registration_date": datetime.now(),
        "photo": avatar,
        "birth_date": request.form.get("birth_date"),
        "geometry": DEFAULT_GEOMETRY if 'geometry' not in request.form else request.form.get("geometry").split(','),
        "eyes": request.form.get("eyes"),
        "gender": request.form.get("gender"),
        "hair": request.form.get("hair"),
        "skin": request.form.get("skin")
    }


def parse_response(response):
    response["gender"] = basic.gender(response["gender"])
    response["hair"] = basic.hair(response["hair"])
    response["eyes"] = basic.eyes(response["eyes"])
    response["skin"] = basic.skin(response["skin"])
    response["geometry"] = ','.join(response["geometry"])
    return response
