def hair(color):
    colors = {
        "black": "Preto",
        "brown": "Castanho",
        "redhead": "Ruivo",
        "blond": "Loiro"
    }
    return colors.get(color)


def eyes(color):
    colors = {
        "green": "Verde",
        "blue": "Azul",
        "brown": "Castanho",
        "black": "Pretos",
        "dark_brown": "Castanho Escuro"

    }
    return colors.get(color)


def skin(color):
    colors = {
        "white": "Branco",
        "black": "Negro",
        "brown": "Pardo",
        "yellow": "Amarelo",
    }
    return colors.get(color)


def gender(g):
    types = {
        "male": "Masculino",
        "female": "Feminino"
    }
    return types.get(g)


def status(s):
    types = {
        "found": "ENCONTRADO",
        "disappeared": "DESAPARECIDO"
    }
    return types.get(s)
