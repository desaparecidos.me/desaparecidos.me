from datetime import datetime

from flask import render_template
import parser.basic_parse as basic
from configuration.cloudinary_configuration import allowed_file, upload_file

DEFAULT_GEOMETRY = [-47.8823, -15.7934]


def parse_request(request):
    if 'photo' not in request.form \
            and 'file' not in request.files \
            and not request.files['file'] \
            and not allowed_file(request.files['file'].filename):
        return render_template("error/image_format.html")

    photo = None

    if 'photo' in request.form:
        photo = request.form.get("photo")

    if 'file' in request.files:
        file = request.files['file']
        photo = upload_file(file, 'missing')["url"]

    return {
        "name": request.form.get("name"),
        "nickname": request.form.get("nickname"),
        "height": request.form.get("height"),
        "clothes": request.form.get("clothes"),
        "gender": request.form.get("gender"),
        "eyes": request.form.get("eyes"),
        "user_id": request.form.get("user_id"),
        "birth_date": request.form.get("birth_date"),
        "event_report": request.form.get("event_report"),
        "hair": request.form.get("hair"),
        "skin": request.form.get("skin"),
        "photo": photo,
        "geometry": DEFAULT_GEOMETRY if 'geometry' not in request.form else request.form.get("geometry").split(','),
        "date_of_disappearance": request.form.get("date_of_disappearance"),
        "status": request.form.get("status"),
        "tattoo_description": request.form.get("tattoo"),
        "scar_description": request.form.get("scar"),
    }


def parse_request_address(request):
    return {
        "observation": request.form.get("observation"),
        "geometry": DEFAULT_GEOMETRY if 'geometry' not in request.form else request.form.get("geometry").split(','),
        "date": datetime.now()
    }


def parse_cursor(users):
    response = []
    for user in users:
        response.append(parse_missing(user))
    return response


def parse_missing(missing):
    missing["gender"] = basic.gender(missing["gender"])
    missing["hair"] = basic.hair(missing["hair"])
    missing["eyes"] = basic.eyes(missing["eyes"])
    missing["skin"] = basic.skin(missing["skin"])
    missing["status"] = basic.status(missing["status"])

    return missing
