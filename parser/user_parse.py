from datetime import datetime

from flask import render_template

from configuration.cloudinary_configuration import allowed_file, upload_file


def user_json(request):
    avatar = None

    if 'file' in request.files:
        file = request.files['file']

        if file and allowed_file(file.filename):
            response = upload_file(file, 'user')
            avatar = response["url"]
        else:
            return render_template("error/image_format.html")
    else:
        if 'avatar' in request.form:
            avatar = request.form.get("avatar")

    return {
        "name": request.form.get('name'),
        "email": request.form.get('email'),
        "registration_date": datetime.now(),
        "accept_terms": request.form.get('accept_terms') == 'on',
        "password": request.form.get('password'),
        "phone": request.form.get('phone'),
        "cell_phone": request.form.get('cell_phone'),
        "avatar": avatar
    }


def parse_cursor_to_user(cursor):
    return {
        "_id": str(cursor["_id"]),
        "name": cursor['name'],
        "email": cursor['email'],
        "password": cursor['password'],
        "phone": cursor['phone'],
        "cell_phone": cursor['cell_phone'],
        "avatar": cursor["avatar"]
    }


def authorization_json(request):
    return {
        "email": request.form.get("email"),
        "password": request.form.get("password")
    }


def remember_password(request):
    return {
        "email": request.form.get("email")
    }
