from flask import Flask

import controllers.custom_errors as custom_erros
from controllers.commons_resource import commons_controller
from controllers.disappeared_resource import missing_controller
from controllers.user_resource import user_controller
from controllers.homeless_resource import homeless_controller

app = Flask(__name__)
app.secret_key = "!@DESAPARECIDOS.ME@!"

app.config['UPLOAD_FOLDER'] = "uploads"

app.register_blueprint(commons_controller)
app.register_blueprint(user_controller, url_prefix="/user")
app.register_blueprint(homeless_controller, url_prefix="/homeless")
app.register_blueprint(missing_controller, url_prefix="/missing")
app.register_error_handler(400, custom_erros.bad_request)
app.register_error_handler(404, custom_erros.page_not_found)
app.register_error_handler(403, custom_erros.access_denied)
app.register_error_handler(409, custom_erros.conflict)
app.register_error_handler(500, custom_erros.generic)
# TODO create page exception

if __name__ == "__main__":
    app.run(debug=True)
