from flask import Blueprint, render_template, request, abort, flash

import services.homeless_service as homeless_service
from configuration.loggin_configuration import logger
from parser.homeless_parse import request_parse, parse_response

homeless_controller = Blueprint("homeless_controller", __name__)


@homeless_controller.route("/create", methods=["GET"])
def create():
    return render_template("homeless/create.html", name="register")


@homeless_controller.route("/register", methods=["POST"])
def register():
    try:
        homeless = request_parse(request)

        result = homeless_service.create_homeless(homeless)

        if result.status_code == 201:
            flash('Cadastro realizado com sucesso!')
            return render_template("homeless/create.html")

        abort(result.status_code)

    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)


@homeless_controller.route("/")
def index():
    try:
        homeless = [parse_response(item) for item in homeless_service.find_all_homeless().json()["homeless"]]
        return render_template("homeless/index.html", homeless=homeless, title="Quero ser encontrado", name="homeless")
    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)
