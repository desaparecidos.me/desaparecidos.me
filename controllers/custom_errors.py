from flask import render_template


def bad_request(e):
    return render_template('error/bad_request.html'), 400


def page_not_found(e):
    return render_template('error/not_found.html'), 404


def access_denied(e):
    return render_template('error/not_found.html'), 403


def conflict(e):
    return render_template('error/conflict.html'), 409


def generic(e):
    return render_template('error/generic.html'), 500
