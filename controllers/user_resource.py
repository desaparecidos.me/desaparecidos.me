from flask import Blueprint, abort, redirect, url_for, render_template, request, session, jsonify, flash
from werkzeug.exceptions import BadRequestKeyError

import parser.user_parse as user_parse
import services.disappeared_service as disappeared_service
import services.sighted_service as sighted_service
import services.user_service as user_service
from configuration.loggin_configuration import logger

user_controller = Blueprint('user_controller', __name__)


@user_controller.route("/create", methods=['GET'])
def create():
    return render_template("user/create.html", title="Cadastre-se", name="register")


@user_controller.route("<_id>/password", methods=["GET"])
def password(_id):
    if 'user' in session and session['user']['_id'] == _id:
        return render_template("user/password.html", title="Alterar senha", _id=_id)

    abort(403)


@user_controller.route("<_id>/change_password", methods=["POST"])
def change_password(_id):
    if 'user' in session and session['user']['_id'] == _id:
        result = user_service.update_password(_id, request.form.get("password"))
        if result.status_code == 200:
            flash("Senha alterada com sucesso!")
            return render_template("user/password.html", title="Alterar senha", _id=_id)

        abort(result.status_code)

    abort(403)


@user_controller.route("/register", methods=['POST'])
def create_user():
    try:
        parse = user_parse.user_json(request)

        result = user_service.create_user(parse)

        if result.status_code in [200, 201]:
            _id = result.json()["user"]["_id"]
            flash("Usuário criado com sucesso!")
            return redirect(url_for('missing_controller.create', _id=_id))

        abort(result.status_code)

    except BadRequestKeyError:
        return render_template("error/image_format.html")


@user_controller.route("/login", methods=["GET"])
def login():
    return render_template("user/login.html")


@user_controller.route("/authentication", methods=["POST"])
def authentication():
    try:
        auth = user_parse.authorization_json(request)

        user = user_service.authentication(auth)

        if user.status_code == 200:
            user_result = user.json()["user"]
            flash("Bem vindo {}".format(user_result["name"]))
            session["user"] = user_result
            return redirect(url_for('missing_controller.index', page=0))

        abort(user.status_code)

    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)


@user_controller.route("/logout", methods=["GET"])
def logout():
    session.pop('user', None)
    return redirect(url_for('missing_controller.index', page=0))


@user_controller.route("/terms/<_id>", methods=['GET'])
def terms(_id):
    return render_template("user/terms.html", _id=_id)


@user_controller.route('/notifications/<_id>', methods=['GET'])
def notifications(_id):
    if 'user' in session and session['user']['_id'] == _id:
        return render_template("user/notifications.html",
                               title="Locais onde foi avistado",
                               _id=_id)

    abort(403)


@user_controller.route('/notifications-list/<_id>', methods=['GET'])
def notifications_list(_id):
    if 'user' in session and session['user']['_id'] == _id:
        disappeared = disappeared_service.get_by_user_id(_id)
        sighted = []
        if disappeared.status_code == 200:
            for missing in disappeared.json()["disappeared"]:
                for sighted_list in sighted_service.find_by_missing(missing["_id"]).json()["sighted"]:
                    sighted.append(sighted_list)

        return jsonify({'locations': sighted})


@user_controller.route("/remember-me", methods=['GET'])
def remember_me():
    return render_template("remember.html")


@user_controller.route('/remember', methods=['POST'])
def remember():
    try:
        result = user_service.remember_password(user_parse.remember_password(request))

        if result.status_code == 200:
            flash("Senha recuperada com sucesso, verifique sua nova senha no seu email!")
            return redirect(url_for("missing_controller.index", page=0))

        if result.status_code == 404:
            abort(404)

    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)


@user_controller.route('/edit/<_id>', methods=['GET'])
def edit(_id):
    try:
        if 'user' in session and session['user']['_id'] == _id:
            user = user_service.find_by_id(_id)
            if user.status_code == 200:
                return render_template("user/edit.html", user=user.json()["user"])

            abort(user.status_code)

        abort(403)

    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)


@user_controller.route('/update/<_id>', methods=['POST'])
def update(_id):
    try:
        if 'user' in session and session['user']['_id'] == _id:
            parse = user_parse.user_json(request)

            result = user_service.update_user(_id, parse)

            if result.status_code == 200:
                flash("Suas informações foram atualizadas com sucesso!")
                return render_template("user/edit.html", user=result.json()["user"], _id=_id)

            abort(result.status_code)

        abort(403)
    except Exception as e:
        logger.error("Error during create homeless", e)
        abort(500)
