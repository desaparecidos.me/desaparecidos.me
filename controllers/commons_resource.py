import services.homeless_service as homeless_service
import services.disappeared_service as disappeared_service
from flask import Blueprint, render_template, redirect, url_for

commons_controller = Blueprint("commons_controller", __name__)


@commons_controller.route("/faq")
def faq():
    return render_template("user/faq.html", title="FAQ", name="faq")


@commons_controller.route("/terms")
def terms():
    return render_template("user/terms.html")


@commons_controller.route("/info")
def info():
    result = dict()
    result["homeless"] = homeless_service.homeless_info().json()["homeless"]
    result["disappeared"] = disappeared_service.disappeared_info().json()["disappeared"]
    return render_template("info.html", title="Em tempo real", result=result, name="info")


@commons_controller.route("/start")
def start():
    return render_template("start_user.html", title="Cadastre-se", name="register")


@commons_controller.route("/")
def index():
    return redirect(url_for('missing_controller.index', page=0))
