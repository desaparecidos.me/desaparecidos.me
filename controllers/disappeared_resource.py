from flask import Blueprint, redirect, url_for, render_template, abort, request, jsonify, flash, session

import services.disappeared_service as missing_service
import services.sighted_service as sighted_service
from configuration.loggin_configuration import logger
from parser.disappeared_parse import parse_request, parse_request_address, parse_missing

missing_controller = Blueprint('missing_controller', __name__)

PATH = "missing"


@missing_controller.route("/search", methods=['POST'])
def search():
    try:
        value = request.form.get("search")
        result = [parse_missing(item) for item in missing_service.search_all(value).json()["disappeared"]]

        return render_template("{}/index.html".format(PATH),
                               disappeared=result,
                               title="Pessoas desaparecidas",
                               pages=None,
                               selected=None,
                               name='missing')

    except Exception as e:
        logger.error("Error during search disappeared ", e)
        abort(500)


@missing_controller.route("/<page>")
def index(page=0):
    try:
        result = missing_service.pagination(page)

        pages = 0
        disappeared = []

        if result.status_code == 200:
            parse = result.json()
            pages = parse["pages"]
            disappeared = [parse_missing(item) for item in parse["disappeared"]]

        return render_template("{}/index.html".format(PATH),
                               disappeared=disappeared,
                               title="Pessoas desaparecidas",
                               pages=pages,
                               selected=int(page),
                               name='missing')

    except Exception as e:
        logger.error("Error during search disappeared ", e)
        abort(500)


@missing_controller.route("/sighted/<_id>/custom", methods=['GET'])
def sighted(_id):
    return render_template("{}/address.html".format(PATH), _id=_id, name="missing")


@missing_controller.route("/sighted/<_id>/custom", methods=['POST'])
def register_sighted(_id):
    try:
        address = parse_request_address(request)
        result = sighted_service.create_sighted(_id, address)

        if result.status_code == 201:
            flash("Registramos sua notificação, obrigado por colaborar.")
            return render_template("{}/address.html".format(PATH), _id=_id, name="missing")

        abort(result.status_code)

    except Exception as e:
        logger.error("Error during search disappeared ", e)
        abort(500)


@missing_controller.route("/maps", methods=['GET'])
def missing_maps():
    return render_template("{}/maps.html".format(PATH), title="Mapa de desaparecimento", name="map")


@missing_controller.route("/list-all-location", methods=['GET'])
def list_all_locations():
    return jsonify({"locations": missing_service.disappeared_geometry().json()["disappeared"]})


@missing_controller.route("/<_id>/create", methods=['GET'])
def create(_id):
    return render_template("{}/create.html".format(PATH), _id=_id, name="register")


@missing_controller.route("/register/<user_id>", methods=['POST'])
def register(user_id):
    try:
        disappeared = parse_request(request)

        result = missing_service.create_missing(user_id, disappeared)

        if result.status_code == 201:
            flash("Desaparecido cadastrado com sucesso!")
            return redirect(url_for('missing_controller.index', page=0))

        abort(result.status_code)

    except Exception as e:
        logger.error("Error during create missing", e)
        abort(500)


@missing_controller.route("/edit/<_id>", methods=["GET"])
def edit(_id):
    try:
        if 'user' in session and session['user']['_id'] == _id:
            missing = missing_service.get_by_user_id(_id)

            if missing.status_code == 200:
                return render_template("missing/edit.html", disappeared_list=missing.json()["disappeared"])

            if missing.status_code == 404:
                return redirect(url_for("missing_controller.create", _id=_id))

            if missing.status_code == 500:
                abort(500)

        abort(403)

    except Exception as e:
        logger.error("Error during search disappeared ", e)
        abort(500)


@missing_controller.route("/update/<_id>", methods=["POST"])
def update(_id):
    try:
        if 'user' in session:
            disappeared = parse_request(request)

            missing = missing_service.update(_id, disappeared)

            if missing.status_code == 200:
                flash("Informações do desaparecido atualizada com sucesso!")
                return redirect(url_for("missing_controller.edit", _id=disappeared["user_id"]))

            abort(missing.status_code)

        abort(403)

    except Exception as e:
        logger.error("Error during search disappeared ", e)
        abort(500)
