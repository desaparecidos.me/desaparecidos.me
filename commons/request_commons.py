import os

import requests
from requests.auth import HTTPBasicAuth
from configuration.loggin_configuration import logger

BASE_URL = os.environ.get("BASE_URL", "http://localhost:5000")
BASE_URI = "{}{}"


def post(uri, data):
    try:
        return requests.post(BASE_URI.format(BASE_URL, uri), data=data,
                             auth=HTTPBasicAuth(os.environ.get("BASIC_AUTH_USERNAME"),
                                                os.environ.get("BASIC_AUTH_PASSWORD")))
    except Exception as e:
        logger.error("Error during create user", e)
        raise e


def put(uri, data):
    try:
        return requests.put(BASE_URI.format(BASE_URL, uri), data=data,
                            auth=HTTPBasicAuth(os.environ.get("BASIC_AUTH_USERNAME"),
                                               os.environ.get("BASIC_AUTH_PASSWORD")))
    except Exception as e:
        logger.error("Error during create user", e)
        raise e


def patch(uri, data):
    try:
        return requests.patch(BASE_URI.format(BASE_URL, uri), data=data,
                              auth=HTTPBasicAuth(os.environ.get("BASIC_AUTH_USERNAME"),
                                                 os.environ.get("BASIC_AUTH_PASSWORD")))
    except Exception as e:
        logger.error("Error during create user", e)
        raise e


def delete(uri):
    try:
        return requests.delete(BASE_URI.format(BASE_URL, uri),
                               auth=HTTPBasicAuth(os.environ.get("BASIC_AUTH_USERNAME"),
                                                  os.environ.get("BASIC_AUTH_PASSWORD")))
    except Exception as e:
        logger.error("Error during create user", e)
        raise e


def get_by_uri(uri):
    try:
        return requests.get(BASE_URI.format(BASE_URL, uri),
                            auth=HTTPBasicAuth(os.environ.get("BASIC_AUTH_USERNAME"),
                                               os.environ.get("BASIC_AUTH_PASSWORD")))
    except Exception as e:
        logger.error("Get from uri %s ", uri, e)
        raise e
