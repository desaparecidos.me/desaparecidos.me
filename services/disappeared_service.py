from urllib.parse import quote

import commons.request_commons as request


def create_missing(user_id, data):
    return request.post("/user/{}/disappeared".format(user_id), data=data)


def remove_missing(_id):
    return request.delete("/missing/{}".format(_id))


def update(_id, disappeared):
    result = request.put("/missing/{}".format(_id), data=disappeared)
    return request.get_by_uri(result.json()["uri"]) if result.status_code in [200, 201] else result


def get_by_id(_id):
    return request.get_by_uri("/missing/{}".format(_id))


def get_by_user_id(user_id):
    return request.get_by_uri("/user/{}/disappeared".format(user_id))


def pagination(page):
    return request.get_by_uri("/disappeared?page={}".format(page))


def count():
    return int(request.get_by_uri("/disappeared?count").json()["count"])


def search_all(text):
    return request.get_by_uri("/disappeared?search={}".format((quote(text))))


def disappeared_geometry():
    return request.get_by_uri("/disappeared?geometry")


def disappeared_info():
    return request.get_by_uri("/disappeared?info")
