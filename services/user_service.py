import commons.request_commons as request


def create_user(data):
    result = request.post("/users", data)
    return request.get_by_uri(result.json()["uri"]) if result.status_code == 201 else result


def update_user(_id, data):
    result = request.put("/user/{}".format(_id), data=data)
    return request.get_by_uri(result.json()["uri"]) if result.status_code in [200, 201] else result


def authentication(auth):
    return request.post("/auth", auth)


def delete_use(_id):
    return request.delete("/user/{}".format(_id))


def remember_password(data):
    return request.patch("/auth/remember", data=data)


def find_by_id(_id):
    return request.get_by_uri("/user/{}".format(_id))


def update_password(_id, password):
    return request.patch("/user/{}".format(_id), data={"password": password})
