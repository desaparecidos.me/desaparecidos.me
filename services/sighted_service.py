import commons.request_commons as request


def create_sighted(missing_id, data):
    return request.post("/missing/{}/sighted".format(missing_id), data=data)


def find_by_missing(_id):
    return request.get_by_uri("/missing/{}/sighted".format(_id))
