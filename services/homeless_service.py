import commons.request_commons as request


def create_homeless(data):
    return request.post("/homeless", data)


def find_all_homeless():
    return request.get_by_uri("/homeless")


def homeless_info():
    return request.get_by_uri("/homeless?info")
